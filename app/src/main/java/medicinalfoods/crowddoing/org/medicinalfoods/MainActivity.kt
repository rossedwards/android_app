package medicinalfoods.crowddoing.org.medicinalfoods

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.content.Intent
import android.view.View;
import kotlinx.android.synthetic.main.activity_main.*
//import com.joanzapata.iconify.Iconify
//import com.joanzapata.iconify.fonts.FontAwesomeModule
import android.content.*
import android.graphics.*
import android.widget.TextView
import android.graphics.Typeface




class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //val fontAwesomeModule = FontAwesomeModule()

        //Iconify
        //    .with(fontAwesomeModule)

        val fontFamily = Typeface.createFromAsset(assets, "fontawesome-webfont.ttf")
        //val sampleText = findViewById(R.id.text) as TextView
        switchActivity.typeface = fontFamily
        switchActivity.text = "\uF0C0"

    }


    fun onClick(view: View) {
        when (view.getId()) {
        //    R.id.changeText -> editText.setText("Lalala")
            R.id.switchActivity -> {
                val intent = Intent(this, MainActivity1::class.java)
                intent.putExtra("input", "editText.getText().toString()")
                startActivity(intent)
            }
        }

    }
}
