package medicinalfoods.crowddoing.org.medicinalfoods

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.content.Intent
import android.view.View;
import android.widget.TextView
import android.util.Log
import kotlinx.android.synthetic.main.activity_main1.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory
//import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
//import jdk.nashorn.internal.objects.NativeFunction.call




class MainActivity1 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main1)

        //val viewById = findViewById<View>(R.id.textView) as TextView
        val inputData = intent.extras
        val input = inputData!!.getString("input")
        textView.text = input

        //val client = OkHttpClient().newBuilder()
        //    .cache(cache)
            //.addInterceptor(LastFmRequestInterceptor(apiKey, cacheDuration))
            //.addInterceptor(HttpLoggingInterceptor().apply {
            //    level = if (BuildConfig.DEBUG) Level.BODY else Level.NONE
            //})
            //.build()

        //companion object {
        //    private const val BASE_URL = "https://api.themoviedb.org/3/movie/"
        //}

        //fun getServiceApi(retrofit : Retrofit) = retrofit.create(ApiService::class.java)

        /*fun getRetrofit() = Retrofit.Builder()
            //.addCallAdapterFactory(CoroutineCallAdapterFactory())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BASE_URL)
            .client(okHttpClient)
            .build()*/

        //val apiService = getServiceApi(getRetrofit())

        //ApiService apiService = ApiService.retrofit.create(ApiService.class)
        //Call<List<Contributor>> call = apiService.searchArtists()
        //List<Contributor> result = call.execute().body()

        val retrofit = Retrofit.Builder()
            .baseUrl("https://roqc0nr8w1.execute-api.us-east-1.amazonaws.com")
            .addConverterFactory(JacksonConverterFactory.create(
                jacksonObjectMapper()
                    //.registerModule(JavaTimeModule())
            ))
            .build()

        val service = retrofit.create(ApiService::class.java)
        //val response = service.searchArtist().execute()
        val response = service.searchArtist()

        response.enqueue(object : Callback<Any> {
            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                val myItem = response.body()

                val TAG = "MainActivity1";

                Log.i(TAG, myItem.toString())
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                //Handle failure
            }
        })

        //val service = retrofit.createService(ApiService::class.java)

        val TAG = "MainActivity1";

        //Log.i(TAG, response.toString())

    }


    //fun onClick(view: View) {
        //when (view.getId()) {
        //    R.id.changeText -> editText.setText("Lalala")
        //    R.id.switchActivity -> {
        //        val intent = Intent(this, SecondActivity::class.java)
        //        intent.putExtra("input", editText.getText().toString())
        //        startActivity(intent)
        //    }
        //}

    //}
}
